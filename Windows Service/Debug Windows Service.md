# 윈도우 서비스 디버깅 방법
## Console application으로 실행해서 디버깅 하는 방법
### 출처
 - [https://docs.microsoft.com/en-us/dotnet/framework/windows-services/how-to-debug-windows-service-applications#how-to-run-a-windows-service-as-a-console-application](https://docs.microsoft.com/en-us/dotnet/framework/windows-services/how-to-debug-windows-service-applications#how-to-run-a-windows-service-as-a-console-application)
### 참고 사항
- 이 방법으로 디버깅하면 현재 Visual Studio를 실행하고 있는 사용자 세션에서 프로그램이 실행된다.
 - 'System/Local Service/Network Service' 사용자로 서비스를 실행시키고 디버깅하기 위해선 다른 방법이 필요할 듯 하다.
### 방법
1. 서비스에 다음과 같이 메소드 추가
```cs
internal void TestStartupAndStop(string[] args)
{
    this.OnStart(args);
    Console.WriteLine("Service started.");
    Console.ReadLine();
    Console.WriteLine("Press ENTER to run OnStop()");
    this.OnStop();
}
```
2. Main을 다음과 같이 수정
```csharp
static void Main(string[] args)
{
    if (Environment.UserInteractive)
    {
        MyNewService service1 = new MyNewService(args);
        service1.TestStartupAndStop(args);
    }
    else
    {
        // Put the body of your old Main method here.
    }
}
```
3. 'Output Type'을 'Console Application'으로 변경
![](Debug01.png)
## 런타임에서 디버거 연결을 요청하는 방법
1. 디버깅을 시작할 부분에 아래와 같이 추가
```cs
protected override void OnStart(string[] args)
{
    #if DEBUG
        System.Diagnostics.Debugger.Launch();
    #endif

    // ...
}
```
2. Debug 모드로 빌드 후 서비스 설치
3. 서비스 시작하고 디버거 연결
![](Debug02.gif)